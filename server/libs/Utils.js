const fs = require('fs')
module.exports = {
  computeConsumption: function(data = [], numberOfDays) {
    let consumption = 0;
    if (data.length !== 0) {
      data.forEach(el => {
        consumption += el.consumption * numberOfDays
      });
      return consumption;
    } else {
      return null;
    }
  },

  getProfiles: function() {
    const home = JSON.parse(fs.readFileSync(process.env.DEV_DB, 'utf-8'))
    const profiles = home.profiles
    return profiles
  }
}
