module.exports = {
  success: function(res, data) {
    return res.status(200).send({
      state: 'success',
      data: data
    })
  },

  error: function(res, errorMessage, errorCode = 500) {
    return res.status(errorCode).send({
      state: 'error',
      errorMesage: errorMessage
    })
  }
}
