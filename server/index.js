var express = require('express');
var app = express();
const bodyParser = require("body-parser");
const Response = require('./libs/Response')
//controllers
const HomeController = require('./controllers/HomeController')
const ProfileController = require('./controllers/ProfileController')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());

//get home
app.route('/').get(HomeController.getHome)

// get consumption
app.route('/consumption').get(HomeController.consumption)

// get profiles
app.route('/profiles').get(ProfileController.getProfiles)

// check profile pin
app.route('/check-profile').post(ProfileController.checkProfile)

// get actual temperature
app.route('/inhousetemp').get(HomeController.inHouseTemp)

// set home temperature
app.route('/changetemp').get(HomeController.setHouseTemp)

// get weather
app.route("/getweather").get(HomeController.getWeather)

// get CCTV footage
app.route("/cctv/:id/footage").get(HomeController.getCctvFootage)

app.get('*', (req, res) => {
  Response.error(res, "Api endpoint doesn't exist.", 404)
})




const PORT = 3000
const HOST = '127.0.0.1';
process.env.DEV_DB = "./server/db/Home.json";

app.listen(PORT, HOST, () => {
  console.log('server running at http://%s:%s', HOST, PORT)
})
