const fs = require('fs')
const Response = require('../libs/Response')
const Utils = require('../libs/Utils')

module.exports = {
  getProfiles: function(req, res) {
    const profiles = Utils.getProfiles()
    try {
      Response.success(
        res,
        profiles
      )
    } catch (error) {
      Response.error(res, error.message)
    }
  },

  checkProfile: function(req, res) {
    const profiles = Utils.getProfiles()
    const profileId = req.body.id
    const profilePin = req.body.pin
    const incomingProfile = profiles[profileId]
    console.log(incomingProfile)
    if (incomingProfile) {
      if(incomingProfile.pin === profilePin) {
        Response.success(res, "Profile pin matched!")
      } else {
        Response.error(res, "Profile pin missmatch!", 403)
      }
    } else {
      Response.error(res, "Profile with this id doesn't exist", 400)
    }

  }
}
