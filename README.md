# Smart Home Control

CTU project for a course Management of Software Projects (B6B36RSP)

It's an application for **SMART TVs**, which offers control of smart homes.

## Fuctionality
// TODO

### Electricity Consumption
For simplication on server side, we represent consumption of each device as **kWh per day**.

#### Usage
- Led-Light, Television: 5 hours per day
- Oven: 2 hours per day

## Server

Server is running on NodeJS and offers lightweight API.
There is no database maintained, all configuration and data of a home will be in a JSON file.

In a development mode a server runs at http://127.0.0.1:3000

### API

API offers multiple actions, which can be called to get some data from JSON file, add some data or make changes.

Response body will always look like this:
```
{
  "state": // "success" or "error",
  "data": // Some data
}
```

#### Endpoints

1. Get a whole home object
    - type: GET
    - route: **/**
    - returns: object with state and home object

2. Get consumption of a whole home, based on given date range
    - type: GET
    - route: **/consumption**
    - returns: object with state and total consumption number
    - request body: date range object (format: YYYY-MM-DD)
    ```
    // example of an date range object
    {
	    "from": "2019-01-01",
	    "to": "2019-03-30"
    }
    ```
3. Get home profiles
    - type: GET
    - route: **/profiles**
    - returns: object with state and object with profiles
    - request body: none

4. Check profile pin
    - type: POST
    - route: **/check-profile**
    - returns: object with state and message
    - request body: object with profile id and input pin
    ```
    // example of an request object
    {
	    "id": "profile_1",
	    "pin": "1111"
    }
    ```
5. Get CCTV footage
    - type: GET
    - route: **/cctv/{id}/footage**
    - returns: object with state and link to the footage
    - request body: none

**In progress...Other endpoints are coming.**

